#define _IMAGE_NAME "cki-project/python"
#define _INSTALL_KCOV 1

/* DO NOT use Dockerfile/Containerfile commands before this line */
#include "setup-from-fedora"

#include "pipeline-fedora"

#include "pipeline"

RUN dnf -y install ansible git-lfs gcc \
    htop \
    postgresql \
    python3-falcon python3-gunicorn python3-tox \
    rsync vim-minimal wget \
    nodejs zeromq-devel

/* test jobs */
RUN dnf -y install beaker-client mailx procps-ng rng-tools tree unzip

/* restraint standalone test runner */
RUN curl --config "${CKI_CURL_CONFIG_FILE}" \
    -o /etc/yum.repos.d/beaker-harness.repo https://beaker-project.org/yum/beaker-harness-Fedora.repo
RUN dnf -y install restraint-client

/* helps to maintain restraint connections to Beaker machines. */
RUN dnf -y install autossh

/* cki-lib dependencies for teiid authentication */
RUN dnf -y install krb5-workstation

/* scripts/beaker_download_logs.py */
RUN dnf -y install python3-requests

/* createrepo jobs */
RUN dnf -y install copr-cli createrepo koji

/* packaging for Amazon Lambda */
RUN dnf -y install zip

/* Install and configure lcov
 * http://ltp.sourceforge.net/coverage/lcov/genhtml.1.php
 * Show yellow for >=25 < 50, green >= 50
 * enable branch coverage
 */
RUN dnf install -y lcov
RUN echo "genhtml_med_limit = 25" > /etc/lcovrc
RUN echo "genhtml_hi_limit = 50" >> /etc/lcovrc
RUN echo "genhtml_branch_coverage = 1" >> /etc/lcovrc
RUN echo "lcov_branch_coverage = 1" >> /etc/lcovrc

/* Recent versions of createrepo_c removed the 'createrepo' executable. :( */
RUN ln -sf /usr/bin/createrepo_c /usr/bin/createrepo

#include "cleanup"
