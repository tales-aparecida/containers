/* These are needed by all pipelines images: python and internal/public builders */

#ifdef _USE_YUM_PKG_MANAGER
ARG RPM_PKG_MANAGER_PIPELINE=yum
#else
ARG RPM_PKG_MANAGER_PIPELINE=dnf
#endif

/* Set the language and ctype for building kernels on RHEL. */
/* Note: installing "glibc-langpack-en" may be required to make locale working
 * on newer containers such as RHEL9, as the language settings are no longer
 * part of glibc-common. If you're having issues with the locale check the
 * package is installed before attempting to set them! For reference, see
 * https://bugzilla.redhat.com/show_bug.cgi?id=1909292
 */
ENV LC_CTYPE="en_US.UTF-8"
ENV LANG="en_US.UTF-8"

/* Extend text/plain to nudge web browsers to show S3 artifacts inline */
COPY files/mime.types /usr/local/etc/

#ifndef _USE_YUM_PKG_MANAGER
/* Install "dnf config-manager" */
RUN $RPM_PKG_MANAGER_PIPELINE -y install \
    dnf-plugins-core
#endif

/* for compilation of pip python3-kerberos */
RUN $RPM_PKG_MANAGER_PIPELINE -y install \
    krb5-devel

/* for compilation of pip lxml */
RUN $RPM_PKG_MANAGER_PIPELINE -y install \
    libxslt-devel

/* for GitLab authentication in pipeline and cache */
RUN $RPM_PKG_MANAGER_PIPELINE -y install \
    jq

/* allow basic search for binaries */
RUN $RPM_PKG_MANAGER_PIPELINE -y install \
    which

/* for extraction of kernel config files from SRPMs */
RUN $RPM_PKG_MANAGER_PIPELINE -y install \
    cpio

/* dealing with diffs and tarballs */
RUN $RPM_PKG_MANAGER_PIPELINE -y install \
    diffutils tar xz

#include "snippet-shellspec"
