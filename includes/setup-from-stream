FROM quay.io/centos/centos:stream/**/_STREAM_VERSION
LABEL maintainer="CKI Project Team <cki-project@redhat.com>"

/* Make sure that if a | fails, the build fail */
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

#include "snippet-envvars"
#include "snippet-certs"
#include "snippet-cki-home"
#include "snippet-curlrc"

/* Add ts from moreutils for timestamping long running tasks. */
COPY files/ts /usr/bin/

/* Install "dnf config-manager" */
RUN dnf -y install dnf-plugins-core

/* Don't put quotes to the URL, it'd break the cpp variable conversion */
RUN dnf -y install \
      https://dl.fedoraproject.org/pub/epel/epel-release-latest-_STREAM_VERSION.noarch.rpm

/* Some very needed repositories come disabled by default. */
#if _STREAM_VERSION == 9
RUN dnf config-manager --set-enabled crb
COPY files/c9s.repo /etc/yum.repos.d/
#endif

/* Upgrade all packages */
RUN dnf -y upgrade
